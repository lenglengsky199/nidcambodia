
import './App.css'
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Homepage from './components/Homepage'
import SearchName from './components/SearchName'
import Navigation from './Header/Navigation'


function App() {

    return (
        <>
            <Navigation />

            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<Homepage />} />
                    <Route path="search/:name" element={<SearchName />} />

                </Routes>
            </BrowserRouter>


        </>
    )
}

export default App
