
import React, { useEffect, useState } from 'react';
import { BsDiscord, BsSearch } from 'react-icons/bs';
import { AiOutlineTwitter, AiFillSetting } from 'react-icons/ai';
import { useNavigate, useSearchParams } from "react-router-dom";
import { FaTelegram } from 'react-icons/fa';


export default function Homepage() {

    const [name, setName] = useState('');
    const [errorMsg, setErrorMsg] = React.useState("");
    const navigate = useNavigate();

    const getName = async () => {
        try {
            let pattern = /^[A-Za-z0-9]*$/;

            if (name === '') {
                setErrorMsg(' Name should be not blank.');
                return;
            }
            if (name.length > 10 || name.length < 3) {
                setErrorMsg(' Name should be more than 3  or less than 10 characters.');
                return;
            }


            if (!pattern.test(name)) {
                setErrorMsg('Name should be letters or with number.');
                return;
            }

            navigate(`/search/${name}`);
        } catch (error) {
            alert('Something went wrong.')
        }
    }

    useEffect(() => {
        setErrorMsg('')
    }, [name])
    return (
        <>
            <main>
                <div className="relative px-6 lg:px-8">
                    <div className=" mx-auto max-w-3xl pt-20 pb-32 sm:pt-20 sm:pb-40">
                        <div>
                            <div>
                                <h1 className="text-4xl font-bold tracking-tight sm:text-center sm:text-6xl">
                                    Non Fungible Token Based Digital Identity
                                </h1>
                                <p className='text-lg sm:text-center sm:text-xl pt-5'>Get your NFT-based ID that can be used to stored varieties of account numbers</p>
                                <div className='flex justify-between grow pt-4'>
                                    <input
                                        className='text-sm font-paragraph w-full rounded-l-full py-4 pl-6 pr-4 border border-zinc-900 focus-visible:outline-none
                                     focus-visible:border-black bg-orange-100 placeholder:text-gray-400
                                      text-black  transition-colors'
                                        type='text'
                                        placeholder='Search Name'
                                        autoComplete='off'
                                        msg={errorMsg}
                                        value={name}
                                        onChange={e => setName(e.target.value)}
                                    />

                                    <button type='button'
                                        onClick={e => getName()}
                                        className='bg-orange-600 hover:bg-blue-500 px-6 rounded-r-full transition-colors text-xl'><BsSearch /></button>
                                </div>
                                <div className=" text-red-600 mt-3">
                                    {errorMsg}
                                </div>
                                <div className="mt-8 flex gap-x-4 sm:justify-center">
                                    <a href='https://discord.gg/w3D2VPuc' target='_blank'> <BsDiscord className='text-3xl ' /></a>
                                    <a href="https://twitter.com/nid_cambodia?s=11&t=DVMRz_vRe9S34zANcBj0QA" target='_blank'> <AiOutlineTwitter className='text-3xl ' /></a>
                                    <a href="https://t.me/nid_cambodia" target='_blank'> <FaTelegram className='text-3xl ' /></a>

                                </div>
                            </div>
                            <div className="absolute inset-x-0 top-[calc(100%-13rem)] -z-10 transform-gpu overflow-hidden blur-3xl sm:top-[calc(100%-30rem)]">
                                <svg
                                    className="relative left-[calc(50%+3rem)] h-[21.1875rem] max-w-none -translate-x-1/2 sm:left-[calc(50%+36rem)] sm:h-[42.375rem]"
                                    viewBox="0 0 1155 678"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                >
                                    <path
                                        fill="url(#ecb5b0c9-546c-4772-8c71-4d3f06d544bc)"
                                        fillOpacity=".3"
                                        d="M317.219 518.975L203.852 678 0 438.341l317.219 80.634 204.172-286.402c1.307 132.337 45.083 346.658 209.733 145.248C936.936 126.058 882.053-94.234 1031.02 41.331c119.18 108.451 130.68 295.337 121.53 375.223L855 299l21.173 362.054-558.954-142.079z"
                                    />
                                    <defs>
                                        <linearGradient
                                            id="ecb5b0c9-546c-4772-8c71-4d3f06d544bc"
                                            x1="1155.49"
                                            x2="-78.208"
                                            y1=".177"
                                            y2="474.645"
                                            gradientUnits="userSpaceOnUse"
                                        >
                                            <stop stopColor="#9089FC" />
                                            <stop offset={1} stopColor="#FF80B5" />
                                        </linearGradient>
                                    </defs>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </>
    );
}
