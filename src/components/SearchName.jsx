import React from 'react';
import { BsDiscord, BsSearch } from 'react-icons/bs';
import { AiOutlineTwitter, AiFillSetting } from 'react-icons/ai';
import { useParams } from "react-router-dom";
import logo from '../assets/img/nidc.jpg'


export default function SearchName() {

    let { name } = useParams();

    return (
        <>
            <main>
                <div className="relative px-6 lg:px-8">
                    {/* <div className="mx-auto max-w-7xl py-6 sm:px-6 lg:px-8"> */}
                    <div className=" mx-auto max-w-3xl pt-20 pb-12  sm:pt-20 ">
                        <div className="box-content h-72 w-72 p-4 border-4 border-white mx-auto bg-gradient-to-l from-orange-400">
                            <div className="mt-2">
                                <img className="h-16 ml-5" src={logo} />
                            </div>
                            <div className="mx-auto my-8 p-4 font-bold text-3xl ">{name}.nid</div>
                        </div>
                    </div>
                    <div>Available: 1000 NID</div>
                </div>
            </main>
        </>
    );
}
